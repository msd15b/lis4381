#LIS4381 - Mobile Application Development
## Matthew Dubin
###Assignment 5 Requirements

*Three Parts:*

1. Create new file structure under AMPPS
2. Create regular expression server-side validation
3. Chapter Questions (Ch 11, 12, 19)

####README.md file should include the following items:
- Screenshots of index.php
- Screenshot of failed server-side validation
- Chapter questions

####Assignment Screenshots:
#####Screenshot of index.php landing page:
![home](images/index.png)

#####Screenshot of Failed Validation of Application without values:
![Screen 1](images/error.png)