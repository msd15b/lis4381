# LIS4381 - Mobile Application Development

## Matthew Spenser Dubin

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial
        (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "A2 README.md file")
    - Create mobile HealthyRecipe App
    - Provide screenshots of mobile application
    - Describe screenshots of mobile application
    - Push to Bitbucket repo
    - Chapter questions

3. [A3 README.md](a3/README.md "A3 README.md file")
    - Create mobile My Event App
    - Provide screenshots of mobile application running
    - Provide screenshots of ERD
    - Push to Bitbucket repo
    - Chapter questions

4. [A4 README.md](a4/README.md "A4 README.md file")
    - Create new file structure for AMPPS localhost
    - Create regular expression client-side validation of input
    - Provide screenshots of passed and failed client-side validation
    - Chapter questions

5. [A5 README.md](a5/README.md "A5 README.md file")
    - Create regular expression server-side validation of input
    - Provide screenshot of failed server-side validation
    - Chapter questions

6. [P1 README.md](p1/README.md "P1 README.md file")
    - Create mobile Business Card
    - Provide screenshots of mobile application running
    - Chapter questions

7. [P2 README.md](p2/README.md "P2 README.md file")
    - Create regular expression server-side validation of input
    - Add edit/delete functionality to A5 files
    - Provide screenshot of several pages
    - Chapter questions