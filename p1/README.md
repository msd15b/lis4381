#LIS4381 - Mobile Application Development
##Matthew Dubin
###Project 1 Requirements

*Three Parts:*

1. Distributed Version Control with Bitbucket
2. Mobile application running
3. Chapter Questions (Ch 7, 8)


####README.md file should include the following items:
- Create mobile My Event app
- Provide screenshots of App
- Chapter questions

####Assignment Screenshots:
#####Screenshot of Screen 1 of Application with values:
![Screen 1](images/page1.png)

#####Screenshot of Screen 1 of Application without values:
![Screen 2](images/page2.png)
