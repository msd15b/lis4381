#LIS4381 - Mobile Application Development
## Matthew Dubin
###Assignment 3 Requirements

*Three Parts:*

1. Distributed Version Control with Bitbucket
2. Mobile application running
3. Chapter Questions (Ch 5, 6)
4. ERD that forward engineers
5. SQL file from ERD

####README.md file should include the following items:
- Create mobile My Event app
- Provide screenshots of App
- Create ERD for petshop
- Provide screenshots of ERD
- Chapter questions

####Links to files:
[a3.mwb](a3.mwb)

####Assignment Screenshots:
#####Screenshot of Screen 1 of Application with values:
![ERD](images/erd.png)

#####Screenshot of Screen 1 of Application without values:
![Screen 1](images/numtickets.png)

#####Screenshot of Screen 1 of Application with values:
![Screen 2](images/ticketprice.png)

