#LIS4381 - Mobile Application Development
## Matthew Dubin
###Assignment 1 Requirements

*Three Parts:*

1. Distributed Version Control with Bitbucket
2. Mobile application running
3. Chapter Questions (Ch 3, 4)

####README.md file should include the following items:
- Create mobile HealthyRecipe App
- Provide screenshots of App
- Push to Bitbucket repo
- Chapter questions

####Assignment Screenshots:
#####Screenshot of Screen 1 of Application:
![Screen 1](images/screen1.png)

#####Screenshot of API Version in Editor:
![Screen 2](images/screen2.png)