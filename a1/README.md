#LIS4381 - Mobile Application Development
## Matthew Dubin
###Assignment 1 Requirements

*Three Parts:*

1. Distributed Version Control with Bitbucket
2. Development Instalations
3. Chapter Questions (Ch 1, 2)

####README.md file should include the following items:
- Screenshot of AMPPS Installation My PHP Installation,
- Screenshot of java Hello,
- Screenshot of running Android Studio: My First App
- git commands with short descriptions
- Bitbucket Repo Links: [this assignment](https://bitbucket.org/msd15b/lis4381/src/master/) and [Bitbucket Station Locations](https://bitbucket.org/msd15b/bitbucketstationlocations/src/master/)

> This is a block quote.
> This is the second paragraph in the block quote.
> 
> Git commands with short Descriptions
>

1.  git init - create new local repository
2.  git status - List the files you've changed and those you still need to add or commit
3.  git add - Add one or more files to staging (index)
4.  git commit - Stores the current contents of the index in a new commit along with a log message from the user
5.  git push - Send changes to the master branch of your remote repository
6.  git pull - Fetch and merge changes on the remote server to your working directory
7.  git branch - List all the branches in your repo, and also tell you what branch you're currently in

####Assignment Screenshots:
#####Screenshot of AMPPS running:
![ampps](images/localhost.png)

#####Screenshot of running java Hello:
![java hello](images/java.png)

#####Screenshot of Android Studio - My First App:
![androidstudio](images/phone.png)
