#LIS4381 - Mobile Application Development
## Matthew Dubin
###Project 2 Requirements

*Three Parts:*

1. Create regular expression server-side validation
2. Build upon A5 files and add edit/delete functionality
3. Chapter Questions (Ch 13, 14)

####README.md file should include the following items:
- Screenshots of index.php
- Screenshot of edit_petstore, edit_petstore_process, home, and rss page
- Final Chapter questions

####Assignment Screenshots:
#####Screenshot of index.php landing page:
![index](images/index.png)

#####Screenshot of edit_petstore.php page:
![edit screen](images/edit_petstore.png)

#####Screenshot of edit_petstore_process.php page:
![edit screen error](images/edit_petstore_process.png)

#####Screenshot of home page:
![home](images/home.png)

#####Screenshot of RSS Feeder page:
![rss](images/rss.png)